package com.shiva.springsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shiva.springsecurity.model.Coupon;
import com.shiva.springsecurity.repo.CouponRepo;

@RestController
@RequestMapping(value="/couponapi")
public class CouponController {
	
	@Autowired
	private CouponRepo couponRepo;
	
	@PostMapping("/coupon")
	public Coupon create(@RequestBody Coupon coupon) {
		
		return couponRepo.save(coupon);
		
	}
	
	@GetMapping("/coupon/{code}")
	public Coupon getCoupon(@PathVariable("code") String code) {
		return couponRepo.findByCode(code);
		
	}
	
	@GetMapping("/coupon")
	public List<Coupon> getCoupons() {
		return couponRepo.findAll();
	}

}
