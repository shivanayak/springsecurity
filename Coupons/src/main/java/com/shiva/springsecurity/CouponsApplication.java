package com.shiva.springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CouponsApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(CouponsApplication.class, args);
	}

}
