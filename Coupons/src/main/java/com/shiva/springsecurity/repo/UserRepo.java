package com.shiva.springsecurity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shiva.springsecurity.model.User;

public interface UserRepo extends JpaRepository<User, Long> {
	User findByEmail(String email);
}
