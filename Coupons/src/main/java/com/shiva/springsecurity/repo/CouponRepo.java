package com.shiva.springsecurity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shiva.springsecurity.model.Coupon;

public interface CouponRepo extends JpaRepository<Coupon, Long> {

	Coupon findByCode(String code);

}
