package com.shiva.springsecurity.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shiva.springsecurity.model.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {


}
